//reference: https://sparen.github.io/ph3tutorials/ph3u1l6.html
//this is the first portion of Orange's boss battle
#TouhouDanmakufu[Single]
#ScriptVersion[3]
#Title["Stage 1 Boss Battle Part 1"]
#Text["Set to Normal Mode"]
#System["./../Orange_System.txt"]
#include "./../../default_system/Default_ShotConst.txt" //default bullet graphics
#include "./../utilities.dnh"

/* GLOBAL VARIABLES */
// let count = 0;  (used only for @MainLoop style of firing bullets)
let obj_boss; //declaration of boss
let obj_scene = GetEnemyBossSceneObjectID(); //needed for spellcards
//holds coordinates to teleport to
let coords_x = [];
let coords_y = [];

/* TASKS (Coroutines) */

//this task generates the random locations that Orange will move to
//four points are generated, for four locations
task MovementLocations{
  loop(5){
    coords_x = coords_x ~ [rand(GetCenterX()+90,GetCenterX()-90)];
    coords_y = coords_y ~ [rand(GetCenterY()-60, GetCenterY()-120)];
  }
}

//task that runs when enemy reaches zero
task TFinalize{
  while(ObjEnemy_GetInfo(obj_boss, INFO_LIFE)>0){yield;} //keep delaying while enemy has life

  //Danmakufu does not automatically add spellcard bonus, so you have to do that yourself
  //if statement checks if bomb was not used and no deaths were caught
  /* uncomment if you wish to make this a spellcard again
  if(ObjEnemyBossScene_GetInfo(obj_scene, INFO_PLAYER_SHOOTDOWN_COUNT) + ObjEnemyBossScene_GetInfo(obj_scene,INFO_PLAYER_SPELL_COUNT )==0){
    AddScore(ObjEnemyBossScene_GetInfo(obj_scene,INFO_SPELL_SCORE));
  }
  */

  //delete enemy and all bullets
  Obj_Delete(obj_boss);
  DeleteShotAll(TYPE_ALL,TYPE_IMMEDIATE);
  SetAutoDeleteObject(true); //all objects spawned will be deleted when the script finishes
  CloseScript(GetOwnScriptID());
  return; //not technically needed
}

//Style #1 of instantiating bullets: 
task MainTask{
  let location_count = 0;
  wait(120);
  while(ObjEnemy_GetInfo(obj_boss,INFO_LIFE)>0){

    //a different danmaku pattern will fire depending on the location
    let angleT = GetAngleToPlayer(obj_boss);
    alternative(location_count)
    case(0){
      loop(16){ //16 sections in a circle
        ascent(i in 2..5){ //composed of 4 layers of increasing speed
          if(Obj_IsDeleted(obj_boss)){return;} //prevent (0,0) errors. (this will be copied everywhere)
          //CreateShotA1 is the basic method for shot types
          //parameters: starting position, speed, angle of firing, graphic, number of delay frames
          CreateShotA1(ObjMove_GetX(obj_boss),ObjMove_GetY(obj_boss), i, angleT, DS_BALL_S_WHITE, 5);
        }
        angleT+=360/16;
      }
    }
    case(1){
      loop(4){ //four ring of circles
        loop(16){ //16 rings for a single circle
          if(Obj_IsDeleted(obj_boss)){return;}
          CreateShotA1(ObjMove_GetX(obj_boss),ObjMove_GetY(obj_boss), 2, angleT, DS_BALL_M_WHITE, 5);
          angleT+=360/16;
        }
        wait(10);
      }
    }
    case(2){
      loop(32){ //32 section in the spiral
        ascent(i in 5..7){ //composed of two layers of increasing speed
          if(Obj_IsDeleted(obj_boss)){return;}
          CreateShotA1(ObjMove_GetX(obj_boss),ObjMove_GetY(obj_boss), i, angleT, DS_BALL_S_WHITE, 5);
          CreateShotA1(ObjMove_GetX(obj_boss),ObjMove_GetY(obj_boss), i, angleT+180, DS_BALL_S_WHITE, 5);
        }
        angleT+=15;
        wait(5);
      }
    }
    case(3){ //something is wrong about this particular spellcard
      let other_angle = angleT;
      loop(8){ //this bullet pattern will have a total of 8 layers
        angleT+=15;
        other_angle-=15;
        loop(8){ //8 parts in a single circle
          if(Obj_IsDeleted(obj_boss)){return;}
          CreateShotA1(ObjMove_GetX(obj_boss)-25,ObjMove_GetY(obj_boss), 4, angleT, DS_BALL_S_WHITE, 5);
          CreateShotA1(ObjMove_GetX(obj_boss)+25,ObjMove_GetY(obj_boss), 4, other_angle, DS_BALL_S_WHITE, 5);
          angleT+=360/8;
          other_angle+=360/8;
        }
        wait(15);
      }
    }
    case(4){
      ascent(i in 1..12){ //twelve bullets in a single column
        ascent(a in 1..4){ //3 columns of bullets on either side of the enemy
          if(Obj_IsDeleted(obj_boss)){return;}
          CreateShotA1(ObjMove_GetX(obj_boss),ObjMove_GetY(obj_boss), i/2, angleT+20+(5*a), DS_BALL_M_WHITE, 5);
          CreateShotA1(ObjMove_GetX(obj_boss),ObjMove_GetY(obj_boss), i/2, angleT-20-(5*a), DS_BALL_M_WHITE, 5);
        } 
      }
    }

    wait(60);
    //determine new trajectory to move to
    ObjMove_SetDestAtFrame(obj_boss, coords_x[location_count], coords_y[location_count], 60); 
   
    location_count = (location_count+1) % 5;
    wait(120); //custom function: wait 30 frames until firing next bullet
  }
}

/* Below is an optional task which adds a check for enemy health before firing bullet
this is required for @MainLoop style code, but is not needed for task style code

task fire{
  if(ObjEnemy_GetInfo(obj_boss,INFO_LIFE)<=0){return;}
  let angleT = GetAngleToPlayer(obj_boss);
  CreateShotA1(ObjMove_GetX(obj_boss),ObjMote_GetY(obj_boss), 2, angleT, DS_BALL_S_RED, 5);
}

*/

@Event{
  //set boss HP, life, and spell bonus
  alternative(GetEventType())
  case(EV_REQUEST_LIFE){
    SetScriptResult(500);
  }
  case(EV_REQUEST_TIMER){
    SetScriptResult(60);
  }
  case(EV_REQUEST_SPELL_SCORE){
    SetScriptResult(1000000);
  }
}

@Initialize{
  //initializes and registers new orange object
  obj_boss = ObjEnemy_Create(OBJ_ENEMY_BOSS);
  ObjEnemy_Regist(obj_boss);

  //move to actual starting position
  //note: last parameter indicates how many frames to execute operation
  ObjMove_SetDestAtFrame(obj_boss, GetCenterX(), 60, 60);

  //tells Danmakufu this is a spellcard (re-enable if you want this to be a spellcard)
  //ObjEnemyBossScene_StartSpell(obj_scene);

  //get tasks running
  MovementLocations;
  TDrawOrange;
  TFinalize;
  MainTask;
}

@MainLoop{
  //resets Orange's hitboxes to the enemy's position
  ObjEnemy_SetIntersectionCircleToShot(obj_boss, ObjMove_GetX(obj_boss), ObjMove_GetY(obj_boss), 32); //enemy hitbox
  ObjEnemy_SetIntersectionCircleToPlayer(obj_boss, ObjMove_GetX(obj_boss), ObjMove_GetY(obj_boss), 24); //player hitbox
  //NOTE: consider setting this to position of player in future

  //Style #2 of instantiating bullets: putting them inside main loop (commented out since I like tasks better)
  /*
    if(count %30 == 0){
      let angleT = GetAngleToPlayer(obj_boss);
      CreateShotA1(ObjMove_GetX(obj_boss),ObjMote_GetY(obj_boss), 2, angleT, DS_BALL_S_RED, 5);
    }
  */

  yield; //ensure coroutines (AKA tasks) run
}
